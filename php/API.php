<?php 
require_once "php/crudapi.php";  
class API {    
    public function API(){
        header('Content-Type: application/JSON');                
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
        case 'GET'://consulta
        	$this->getTodo();
            //echo 'GET';
            break;     
        case 'POST'://inserta
            $this->insert();
            //echo 'POST';
            break;                
        case 'PUT'://actualiza
            $this->update();
            //echo 'PUT';
            break;      
        case 'DELETE'://elimina
            $this->delete();
            //echo 'DELETE';
            break;
        default://metodo NO soportado
            echo 'METODO NO SOPORTADO';
            break;
        }
    }

    /**
 * Respuesta al cliente
 * @param int $code Codigo de respuesta HTTP
 * @param String $status indica el estado de la respuesta puede ser "success" o "error"
 * @param String $message Descripcion de lo ocurrido
 */
 function response($code=200, $status="", $message="") {
    http_response_code($code);
    if( !empty($status) && !empty($message) ){
        $response = array("status" => $status ,"message"=>$message);  
        echo json_encode($response,JSON_PRETTY_PRINT);    
    }            
 }

 /**
 * función que segun el valor de "action" e "id":
 *  - mostrara una array con todos los registros de personas
 *  - mostrara un solo registro 
 *  - mostrara un array vacio
 */
 function getTodo(){
     if($_GET['action']=='consulta'){      
         $db = new crudapi();
         if(isset($_GET['id'])){//muestra 1 solo registro si es que existiera ID                 
             $response = $db->getStudents($_GET['id']);                
             echo json_encode($response,JSON_PRETTY_PRINT);
         }else{ //muestra todos los registros                   
             $response = $db->getStudents();              
             echo json_encode($response,JSON_PRETTY_PRINT);
         }
     }else{
            $this->response(400);
     }   
} 

function getTodo2(){ 
    if($_GET['action']=='consulta'){ 
         $db = new crudapi();              
         $response = $db->getStudentss();                
         echo json_encode($response,JSON_PRETTY_PRINT);
     }else{
            $this->response(400);
     } 
} 

 function insert(){
    if($_GET['action']=='insert'){
        if(isset($_GET['id'])){//muestra 1 solo registro si es que existiera ID                 
             $primerNombre = "kakakak"; //$_POST['primerNombre'];
             $segundoNombre = "kakakak"; //$_POST['segundoNombre'];
             $primerApellido = "kakakak"; //$_POST['primerApellido'];
             $segundoApellido = "kakakak"; //$_POST['segundoApellido'];
             $fechaNacimiento = "kakakak"; //$_POST['fechaNacimiento'];
             $db = new crudapi();
             $response = $db->insert($primerNombre, $segundoNombre, $primerApellido, $segundoApellido, $fechaNacimiento);                
             echo json_encode($response,JSON_PRETTY_PRINT);
         }else{ //muestra todos los registros                   
             $response = $db->getStudents();              
             echo json_encode($response,JSON_PRETTY_PRINT);
         }
     }else{
        $this->response(400);
     } 
}  


 function delete(){
     if($_GET['action']=='delete'){      
         $db = new crudapi();
         if(isset($_GET['id'])){//muestra 1 solo registro si es que existiera ID                 
             $response = $db->delete($_GET['id']);                
             echo json_encode($response,JSON_PRETTY_PRINT);
         }
     }else{
            $this->response(400);
     }   
}  

 function update(){
     if($_GET['action']=='put'){      
         $db = new crudapi();
         if(isset($_GET['id'])){//muestra 1 solo registro si es que existiera ID
             $id = $_GET['id'];
             $primerNombre = "lolol"; //$_POST['primerNombre'];
             $segundoNombre = "lolol"; //$_POST['segundoNombre'];
             $primerApellido = "lolol"; //$_POST['primerApellido'];
             $segundoApellido = "lolol"; //$_POST['segundoApellido'];
             $fechaNacimiento = "lolol"; //$_POST['fechaNacimiento'];

             $response = $db->update($id, $primerNombre, $segundoNombre, $primerApellido, $segundoApellido, $fechaNacimiento);                               
             echo json_encode($response,JSON_PRETTY_PRINT);
         }else{ //muestra todos los registros                   
             $response = $db->getStudents();              
             echo json_encode($response,JSON_PRETTY_PRINT);
         }
     }else{
            $this->response(400);
     }   
}  
}//end class
?>