<?php
class CrudApi {
    
   protected $mysqli;
    const LOCALHOST = '127.0.0.1';
    const USER = 'root';
    const PASSWORD = 'root';
    const DATABASE = 'webrest';
    
    /**
     * Constructor de clase
     */
    public function __construct() {           
        try{
            //conexión a base de datos
            $this->mysqli = new mysqli(self::LOCALHOST, self::USER, self::PASSWORD, self::DATABASE);
        }catch (mysqli_sql_exception $e){
            //Si no se puede realizar la conexión
            http_response_code(500);
            exit;
        }     
    } 
    
    /**
     * obtiene un solo registro dado su ID
     * @param int $id identificador unico de registro
     * @return Array array con los registros obtenidos de la base de datos
     */
    public function getStudents($id=0){      
        $stmt = $this->mysqli->prepare("SELECT * FROM alumnos WHERE id_alumnos = ?");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();        
        $alumnos = $result->fetch_all(MYSQLI_ASSOC); 
        $stmt->close();
        return $alumnos;              
    }
    
    public function getStudentss(){      
        $stmt = $this->mysqli->prepare("SELECT * FROM alumnos");
        $stmt->execute();
        $result = $stmt->get_result();        
        $alumnos = $result->fetch_all(MYSQLI_ASSOC); 
        $stmt->close();
        return $alumnos;              
    }

    /**
     * añade un nuevo registro en la tabla persona
     * @param String $name nombre completo de persona
     * @return bool TRUE|FALSE 
     */
    public function insert($primerNombre=0, $segundoNombre=0, $primerApellido=0, $segundoApellido=0, $fechaNacimiento=0){
        $stmt = $this->mysqli->prepare("INSERT INTO alumnos(primerNombre, segundoNombre, primerApellido, segundoApellido, fechaNacimiento) VALUES (?,?,?,?,?)");
        $stmt->bind_param('sssss', $primerNombre, $segundoNombre, $primerApellido, $segundoApellido, $fechaNacimiento);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    /**
     * elimina un registro dado el ID
     * @param int $id Identificador unico de registro
     * @return Bool TRUE|FALSE
     */
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM alumnos WHERE id_alumnos = ?");
        $stmt->bind_param('s', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    /**
     * Actualiza registro dado su ID
     * @param int $id Description
     */
    public function update($id=0, $primerNombre=0, $segundoNombre=0, $primerApellido=0, $segundoApellido=0, $fechaNacimiento=0) {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare("UPDATE alumnos SET primerNombre=?, segundoNombre=?, primerApellido=?, segundoApellido=?, fechaNacimiento=? WHERE id_alumnos = ? ; ");
            $stmt->bind_param('ssssss', $primerNombre, $segundoNombre, $primerApellido, $segundoApellido, $fechaNacimiento, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    /**
     * verifica si un ID existe
     * @param int $id Identificador unico de registro
     * @return Bool TRUE|FALSE
     */
    public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM alumnos WHERE id_alumnos =?");
        $stmt->bind_param("s", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
    
}